//
//  CarCell.m
//  IQCarProject
//
//  Created by CustomSoft on 16/03/15.
//  Copyright (c) 2015 CustomSoft. All rights reserved.
//

#import "CarCell.h"
#import "Utils.h"

@implementation CarCell
@synthesize manufacturerModel, number, line, arrow;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.manufacturerModel = [[UILabel alloc] init];
//        self.manufacturerModel.numberOfLines = 0;
//        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        self.manufacturerModel.font = [Utils getMainFontWithSize:20];
        self.manufacturerModel.textAlignment = NSTextAlignmentLeft;
        self.manufacturerModel.textColor = [Utils colorFromHex:DarkGrayCOLOR];
        self.manufacturerModel.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.number = [[UILabel alloc] init];
//        self.number.numberOfLines = 0;
//        self.number.lineBreakMode = NSLineBreakByWordWrapping;
        self.number.font = [Utils getMainFontWithSize:12];
        self.number.textAlignment = NSTextAlignmentLeft;
        self.number.textColor = [Utils colorFromHex:DarkGrayCOLOR];
        self.number.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.contentView addSubview:self.number];
        
        self.line = [[UIView alloc] init];
        self.line.backgroundColor =  [Utils colorFromHex:DarkGrayCOLOR];
        self.line.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.line];
        
        self.arrow = [[UIImageView alloc] init];
        self.arrow.image = [UIImage imageNamed:@"arrow"];
        self.arrow.contentMode = UIViewContentModeScaleAspectFit;
        self.arrow.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.arrow];

        
        [self setupLayout];
    }
    
    return self;
}

- (void)setupLayout
{
    UIView *leftView = [[UIView alloc] init];
    //leftView.backgroundColor = [UIColor redColor];
    leftView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:leftView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(leftView, line, arrow);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[leftView]-[line(==1)]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[arrow]-[line]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[leftView]-[arrow(==10)]-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:dict]];
    
    [leftView addSubview:self.manufacturerModel];
    [leftView addSubview:self.number];
    
    NSDictionary *left = NSDictionaryOfVariableBindings(manufacturerModel, number);
    
    [leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[manufacturerModel]-[number]" options:0 metrics:nil views:left]];
    [leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[manufacturerModel]-|" options:0 metrics:nil views:left]];
    [leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[number]-|" options:0 metrics:nil views:left]];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
