//
//  TipsViewController.h
//  PuntersPage
//
//  Created by Stefan Oancea on 15/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMXMLParser.h"

@interface TipsViewController : UIViewController

@property (strong, nonatomic) NSArray *dataArray;

@end
