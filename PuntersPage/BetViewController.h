//
//  BetViewController.h
//  PuntersPage
//
//  Created by Stefan Oancea on 20/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BetViewController : UIViewController

@property (strong, nonatomic) NSArray *dataArray;
@property (nonatomic) NSString *bettitle;



@end
