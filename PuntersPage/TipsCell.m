//
//  TipsViewCell.m
//  PuntersPage
//
//  Created by Stefan Oancea on 17/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "TipsCell.h"

@implementation TipsCell
@synthesize tipImage,tipName,tipSubtitle,arrow,line;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.tipName = [[UILabel alloc] init];
        //        self.manufacturerModel.numberOfLines = 0;
        //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.tipName.textAlignment = NSTextAlignmentLeft;
        self.tipName.textColor = [UIColor blackColor];
        self.tipName.font = [UIFont fontWithName:@"Arial-BoldMT" size:17];
        self.tipName.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.tipSubtitle = [[UILabel alloc] init];
        //        self.manufacturerModel.numberOfLines = 0;
        //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.tipSubtitle.textAlignment = NSTextAlignmentLeft;
        self.tipSubtitle.textColor = [UIColor lightGrayColor];
        self.tipSubtitle.translatesAutoresizingMaskIntoConstraints = NO;
        self.tipSubtitle.font = [UIFont fontWithName:@"ArialMT" size:12];
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.tipImage = [[UIImageView alloc] init];
        //self.moreImage.image = [UIImage imageNamed:@"arrow"];
        self.tipImage.contentMode = UIViewContentModeScaleAspectFit;
        self.tipImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.tipImage];
        
        
        
        self.line = [[UIView alloc] init];
        self.line.backgroundColor =  [UIColor lightGrayColor];
        self.line.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.line];
        
        self.arrow = [[UIImageView alloc] init];
        self.arrow.image = [UIImage imageNamed:@"arrow"];
        self.arrow.contentMode = UIViewContentModeScaleAspectFit;
        self.arrow.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.arrow];
        
        
        
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    UIView *leftView = [[UIView alloc] init];
    //leftView.backgroundColor = [UIColor redColor];
    leftView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:leftView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(leftView,line,arrow);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[leftView]-[line(==1)]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[arrow]-[line]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[leftView]-[arrow(==10)]-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:dict]];
    
    [leftView addSubview:self.tipName];
    [leftView addSubview:self.tipSubtitle];
    [leftView addSubview:self.tipImage];
    //  [leftView addSubview:self.betPrice];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:tipImage attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tipImage]-10-[tipName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipImage,tipName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-75-[tipName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-75-[tipSubtitle]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipSubtitle)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[tipImage]-2-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipImage)]];
    
    //    NSDictionary *left = NSDictionaryOfVariableBindings(betName, betSubtitle);
    //
    //    [leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[betName]-0-[betSubtitle]|" options:0 metrics:nil views:left]];
    //
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[tipName]-13-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[tipSubtitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tipSubtitle)]];
    
    
    
    
    
}










- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
