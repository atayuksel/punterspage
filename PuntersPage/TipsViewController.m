//
//  TipsViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 15/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "TipsViewController.h"
#import "TipsCell.h"
#import "SWRevealViewController.h"
#import "TipDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SGImageCache.h"
#import <Netmera/Netmera.h>



@import CoreGraphics;

@interface TipsViewController () <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *tips;
    NSInteger _presentedRow;
    bool contor;
}

-(IBAction)pushExample:(id)sender;

@end

@implementation TipsViewController
@synthesize dataArray = _dataArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    NSURL *feedURL = [NSURL URLWithString:@"http://178.62.49.240/backend/web/ws/tips"];
    KMXMLParser *parser = [[KMXMLParser alloc] initWithURL:feedURL delegate:self];
    //To get the result and store it in an array call the parser 'posts' method
    self.dataArray = [parser posts];
     [self performSelector:@selector(onTick) withObject:nil afterDelay:0.2];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [tips reloadData];
    
    [NMEvent sendEventWithKey:@"tipsclick" value:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    [tips reloadData];
    NSLog(@"did appear");
    
    
}


-(void)setupLayout
{
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];

    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title= @"Tips";
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.view addSubview:navBar];
    
    //add the components to the view
    [view addSubview: navBar];
    
    
    
    
    
    
    
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    revealButtonItem.tintColor = [UIColor whiteColor];
    
    
    tips = [[UITableView alloc] init];
    tips.delegate = self;
    tips.dataSource = self;
    tips.showsVerticalScrollIndicator = true;
    tips.separatorStyle = UITableViewCellSeparatorStyleNone;
    tips.translatesAutoresizingMaskIntoConstraints = NO;
    view=tips;
    
   
    //show the view
    self.view = view;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Example Code

- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TipsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
  //  NSInteger row=indexPath.row;
    

    
    if (cell == nil)
    {
        cell = [[TipsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //  [[cell textLabel] setText:@"Ceva"];
    //  cell.moreName.text=@"ceva";
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    NSString * urlString = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    NSRange range = [urlString rangeOfString:@"%0A%09%09"];
    
    if (range.location != NSNotFound) {
        urlString = [urlString stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                                         withString:@""];
        
    }
    
    
    
//    [imgView sd_setImageWithURL:[NSURL URLWithString:urlString]
//               placeholderImage:[UIImage imageNamed:@"placeholder.png"]
//                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
//     {
//         NSLog(@"Cacheing succesful");
//         
//     }
//     ];
    
    [SGImageCache getImageForURL:urlString].then(^(UIImage *image) {
        if (image) {
            NSLog(@"in image statement");
            imgView.image = image;
            cell.tipImage.image=imgView.image;
        }
    });
    
    
    cell.tipName.text = [[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    cell.tipSubtitle.text= [[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"sub-title"];
    
//    if (contor==NO && indexPath.row == [self.dataArray count] - 1)
//    {
//        [tips reloadData];
//        contor=YES;
//    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    TipDetailViewController *TDVC = [[TipDetailViewController alloc]init];
    
    //TDVC.bettitle=[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"] forKey:@"tiptitle"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"image"] forKey:@"tiplogo"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"url"] forKey:@"tiplink"];
    
//    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"minimum-deposit"] forKey:@"minimum-deposit"];
    
//    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"free-bet"] forKey:@"free-bet"];
//    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"bet-url"] forKey:@"tipactionUrl"];
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"text"] forKey:@"tiptext"];
    

    
    
    
    [self.navigationController pushViewController:[TipDetailViewController new] animated:YES];


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    return 80;
}

- (void)parserDidFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}

- (void)parserCompletedSuccessfully
{
    NSLog(@"Parse complete. You may need to reload the table view to see the data.");
    
    [tips reloadData];
    
}

- (void)parserDidBegin
{
    NSLog(@"Parsing has begun");
}

-(void)onTick
{
    [tips reloadData];
}


@end
