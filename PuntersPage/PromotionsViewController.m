//
//  PromotionsViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 15/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "PromotionsViewController.h"
#import "PromotionCell.h"
#import "SWRevealViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PromDetailViewController.h"
#import "SGImageCache.h"
#import <Netmera/Netmera.h>

@import CoreGraphics;

@interface PromotionsViewController () <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *proms;
    NSInteger _presentedRow;
    UIButton *seeExistingPromotions;
    UIButton *seeNewPromotions;
    bool contor;
}

-(IBAction)pushExample:(id)sender;

@end

@implementation PromotionsViewController



-(void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    NSURL *feedURL = [NSURL URLWithString:@"http://178.62.49.240/backend/web/ws/promotions"];
    KMXMLParser *parser = [[KMXMLParser alloc] initWithURL:feedURL delegate:self];
    //To get the result and store it in an array call the parser 'posts' method
    self.dataArray = [parser posts];
    [self performSelector:@selector(onTick) withObject:nil afterDelay:0.5];
    
    [NMEvent sendEventWithKey:@"promotionsclick" value:nil];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    [proms reloadData];
    
}

-(void)setupLayout
{
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];

    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title= @"Promotions";
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.view addSubview:navBar];
    
    //add the components to the view
    [view addSubview: navBar];
    
    
    
    
    
    
    
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    revealButtonItem.tintColor = [UIColor whiteColor];
    
    proms = [[UITableView alloc] init];
    proms.delegate = self;
    proms.dataSource = self;
    proms.showsVerticalScrollIndicator = true;
    proms.separatorStyle = UITableViewCellSeparatorStyleNone;
    proms.translatesAutoresizingMaskIntoConstraints = NO;
    view=proms;
    
//    seeExistingPromotions = [[UIButton alloc] initWithFrame:CGRectMake(160, 70, 150, 50)];
//    seeExistingPromotions.tag = 1;
//   // seeExistingPromotions.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
//    [seeExistingPromotions setTitle:@"EXISTING" forState:UIControlStateNormal];
//    [seeExistingPromotions setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    //[seeClosedRequests setBackgroundColor:[UIColor blackColor]];
//       [seeExistingPromotions addTarget:self action:@selector(onSeeExistingPromotionsPressed:) forControlEvents:UIControlEventTouchUpInside];
//    seeExistingPromotions.translatesAutoresizingMaskIntoConstraints = NO;
//    [view addSubview:seeExistingPromotions];
//    
//    seeNewPromotions = [[UIButton alloc] initWithFrame:CGRectMake(30, 70, 120, 50)];
//    seeNewPromotions.tag = 2;
//   // seeNewPromotions.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 5);
//    [seeNewPromotions setTitle:@"NEW" forState:UIControlStateNormal];
//    [seeNewPromotions setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [seeNewPromotions addTarget:self action:@selector(onSeeNewPromotionsPressed:) forControlEvents:UIControlEventTouchUpInside];
//    seeNewPromotions.translatesAutoresizingMaskIntoConstraints = NO;
//    [view addSubview:seeNewPromotions];

    
    //show the view
    self.view = view;

}
    
    
    
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)onSeeExistingPromotionsPressed:(id)sender
//{
//    seeExistingPromotions.tag = 2;
//    [seeExistingPromotions setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [seeNewPromotions setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    // [seeExistingPromotions setBackgroundColor:[UIColor greenColor]];
//    
//    seeNewPromotions.tag = 1;
//  //  [seeNewPromotions setBackgroundColor:[UIColor lightGrayColor]];
//    
//    [proms reloadData];
//    
//    
//}
//
//- (void)onSeeNewPromotionsPressed:(id)sender
//{
//    seeExistingPromotions.tag = 1;
//    [seeExistingPromotions setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    [seeNewPromotions setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//
//   // [seeExistingPromotions setBackgroundColor:[UIColor lightGrayColor]];
//    
//    seeNewPromotions.tag = 2;
//   // [seeNewPromotions setBackgroundColor:[UIColor greenColor]];
//    
//    [proms reloadData];
//}

#pragma mark - Example Code

- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
   return [self.dataArray count];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PromotionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
  //  NSInteger row=indexPath.row;
    
        
    
    if (cell == nil)
    {
        cell = [[PromotionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    NSString * urlString = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    NSRange range = [urlString rangeOfString:@"%0A%09%09"];
    
    if (range.location != NSNotFound) {
        urlString = [urlString stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                                         withString:@""];
        
    }
    
    
    
//    [imgView sd_setImageWithURL:[NSURL URLWithString:urlString]
//               placeholderImage:[UIImage imageNamed:@"placeholder.png"]
//                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
//     {
//         NSLog(@"Cacheing succesful");
//         
//     }
//     ];
//    cell.promImage.image=imgView.image;
    
    [SGImageCache getImageForURL:urlString].then(^(UIImage *image) {
        if (image) {
            NSLog(@"in image statement");
            imgView.image = image;
            cell.promImage.image=imgView.image;
        }
    });
    
    cell.promName.text = [[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    cell.promSubtitle.text= [[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"sub-title"];
            
//    if (contor==NO && indexPath.row == [self.dataArray count] - 1)
//    {
//        [proms reloadData];
//        contor=YES;
//    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PromDetailViewController *TDVC = [[PromDetailViewController alloc]init];
    
    //TDVC.bettitle=[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"] forKey:@"promtitle"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"image"] forKey:@"promlogo"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"url"] forKey:@"promlink"];
    
    //    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"minimum-deposit"] forKey:@"minimum-deposit"];
    
    //    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"free-bet"] forKey:@"free-bet"];
    //
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"bet-url"] forKey:@"promactionUrl"];
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"text"] forKey:@"promtext"];
    
    
    
    
    
    [self.navigationController pushViewController:[PromDetailViewController new] animated:YES];
    
    
}





- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    return 80;
}

- (void)parserDidFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}

- (void)parserCompletedSuccessfully
{
    NSLog(@"Parse complete. You may need to reload the table view to see the data.");
    
    
    
}

- (void)parserDidBegin
{
    NSLog(@"Parsing has begun");
}

-(void)onTick
{
    [proms reloadData];
}



@end
