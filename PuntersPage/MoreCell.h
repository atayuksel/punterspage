//
//  MoreCell.h
//  PuntersPage
//
//  Created by Stefan Oancea on 16/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreCell : UITableViewCell

@property(nonatomic,retain) UIImageView *moreImage;
@property(nonatomic,retain) UILabel *moreName;
@property(nonatomic, retain) UIView *line;
@property(nonatomic, retain) UIImageView *arrow;

@end
