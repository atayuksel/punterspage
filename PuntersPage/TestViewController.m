//
//  TestViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 16/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "TestViewController.h"
#import "SWRevealViewController.h"
#import "BetCell.h"

@interface TestViewController () <UITableViewDataSource, UITableViewDelegate>

{
    UITableView *cars;
}
@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupLayout
{

    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];

    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title= @"Test";
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.view addSubview:navBar];
    
    //add the components to the view
    [view addSubview: navBar];
    
    
    
    
    
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    revealButtonItem.tintColor = [UIColor whiteColor];
    
    
    
    cars = [[UITableView alloc] init];
    cars.delegate = self;
    cars.dataSource = self;
    cars.showsVerticalScrollIndicator = true;
    cars.separatorStyle = UITableViewCellSeparatorStyleNone;
    cars.translatesAutoresizingMaskIntoConstraints = NO;
    view=cars;


    
    //show the view
    self.view = view;
    
}



- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 BetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil)
    {
        cell = [[BetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
  //  [[cell textLabel] setText:@"Ceva"];
    cell.betName.text=@"ceva";
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    imgView.image = [UIImage imageNamed:@"reveal-icon.png"];
    cell.imageView.image = imgView.image;
//    cell.number.text = [[carList objectAtIndex:indexPath.row] registrationNumber];
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NewCarViewController *seeCar = [[NewCarViewController alloc] init];
//    [seeCar setMyCar:[carList objectAtIndex:indexPath.row]];
//    [self.navigationController pushViewController:seeCar animated:YES];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{

    
    return 50;
}


@end
