//
//  PromotionCell.m
//  PuntersPage
//
//  Created by Stefan Oancea on 17/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "PromotionCell.h"

@implementation PromotionCell
@synthesize promImage,promName,promSubtitle,arrow,line;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.promName = [[UILabel alloc] init];
        //        self.manufacturerModel.numberOfLines = 0;
        //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.promName.textAlignment = NSTextAlignmentLeft;
        self.promName.textColor = [UIColor blackColor];
        self.promName.font = [UIFont fontWithName:@"Arial-BoldMT" size:17];
        self.promName.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.promSubtitle = [[UILabel alloc] init];
        //        self.manufacturerModel.numberOfLines = 0;
        //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.promSubtitle.textAlignment = NSTextAlignmentLeft;
        self.promSubtitle.textColor = [UIColor lightGrayColor];
        self.promSubtitle.translatesAutoresizingMaskIntoConstraints = NO;
        self.promSubtitle.font = [UIFont fontWithName:@"ArialMT" size:12];
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.promImage = [[UIImageView alloc] init];
        //self.moreImage.image = [UIImage imageNamed:@"arrow"];
        self.promImage.contentMode = UIViewContentModeScaleAspectFit;
        self.promImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.promImage];
        
        
        
        
        self.line = [[UIView alloc] init];
        self.line.backgroundColor =  [UIColor lightGrayColor];
        self.line.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.line];
        
        self.arrow = [[UIImageView alloc] init];
        self.arrow.image = [UIImage imageNamed:@"arrow"];
        self.arrow.contentMode = UIViewContentModeScaleAspectFit;
        self.arrow.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.arrow];
        
        
        
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    UIView *leftView = [[UIView alloc] init];
    //leftView.backgroundColor = [UIColor redColor];
    leftView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:leftView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(leftView,line,arrow);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[leftView]-[line(==1)]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[arrow]-[line]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[leftView]-[arrow(==10)]-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:dict]];
    
    [leftView addSubview:self.promName];
    [leftView addSubview:self.promSubtitle];
    [leftView addSubview:self.promImage];
    //  [leftView addSubview:self.betPrice];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:promImage attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[promImage]-10-[promName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(promImage,promName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-75-[promName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(promName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-75-[promSubtitle]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(promSubtitle)]];
   
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[promImage]-2-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(promImage)]];
    
    //    NSDictionary *left = NSDictionaryOfVariableBindings(betName, betSubtitle);
    //
    //    [leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[betName]-0-[betSubtitle]|" options:0 metrics:nil views:left]];
    //
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[promName]-13-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(promName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[promSubtitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(promSubtitle)]];
    
    
    
    
    
}






- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
