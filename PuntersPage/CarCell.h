//
//  CarCell.h
//  IQCarProject
//
//  Created by CustomSoft on 16/03/15.
//  Copyright (c) 2015 CustomSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarCell : UITableViewCell

@property(nonatomic, retain) UILabel *manufacturerModel;
@property(nonatomic, retain) UILabel *number;
@property(nonatomic, retain) UIView *line;
@property(nonatomic, retain) UIImageView *arrow;

@end
