//
//  BetPaper.m
//  PuntersPage
//
//  Created by Stefan Oancea on 14/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "BetPaper.h"

@implementation BetPaper

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"type": @"typeDetail",
                                                       @"due_date": @"dueDate",
                                                       @"date": @"startDate"
                                                       }];
}



@end
