//
//  BetCell.m
//  PuntersPage
//
//  Created by Stefan Oancea on 14/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "BetCell.h"

@implementation BetCell
@synthesize betImage,betName,betPrice,betSubtitle,arrow,line;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.betName = [[UILabel alloc] init];
        //        self.manufacturerModel.numberOfLines = 0;
        //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.betName.textAlignment = NSTextAlignmentLeft;
        self.betName.textColor = [UIColor blackColor];
        self.betName.font = [UIFont fontWithName:@"Arial-BoldMT" size:17];
        self.betName.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.betSubtitle = [[UILabel alloc] init];
            //        self.manufacturerModel.numberOfLines = 0;
            //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
            
        self.betSubtitle.textAlignment = NSTextAlignmentLeft;
        self.betSubtitle.textColor = [UIColor lightGrayColor];
        self.betSubtitle.translatesAutoresizingMaskIntoConstraints = NO;
        self.betSubtitle.font = [UIFont fontWithName:@"ArialMT" size:12];

        
        self.betImage = [[UIImageView alloc] init];
        self.betImage.contentMode = UIViewContentModeScaleAspectFit;
        self.betImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.betImage];
        
        self.betPrice = [[UILabel alloc] init];
        self.betPrice.textAlignment = NSTextAlignmentLeft;
        float rd = 59.00/255.00;
        float gr = 210.00/255.00;
        float bl = 105.00/255.00;
        [betPrice setTextColor:[UIColor colorWithRed:rd green:gr blue:bl alpha:1.0]];
        self.betPrice.font = [UIFont fontWithName:@"Arial-BoldMT" size:32];
        self.betPrice.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.betPrice];
        
        
        
        self.line = [[UIView alloc] init];
        self.line.backgroundColor =  [UIColor lightGrayColor];
        self.line.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.line];
        
        self.arrow = [[UIImageView alloc] init];
        self.arrow.image = [UIImage imageNamed:@"arrow"];
        self.arrow.contentMode = UIViewContentModeScaleAspectFit;
        self.arrow.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.arrow];
        
        
        
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    UIView *leftView = [[UIView alloc] init];
    //leftView.backgroundColor = [UIColor redColor];
    leftView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:leftView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(leftView,line,arrow,betPrice);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[leftView]-[line(==1)]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[arrow]-[line]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[leftView]-[betPrice(==80)]-[arrow(==10)]-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:dict]];
    
    [leftView addSubview:self.betName];
    [leftView addSubview:self.betSubtitle];
   // [leftView addSubview:self.betImage];
  //  [leftView addSubview:self.betPrice];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:betImage attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
   
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:betPrice attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

//    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[betImage]-10-[betName]-[betPrice]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betImage,betName,betPrice)]];
     [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[betImage]-10-[betName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betImage,betName)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-65-[betName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-65-[betSubtitle]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betSubtitle)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[betPrice]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betPrice)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[betImage]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betImage)]];
  
//    NSDictionary *left = NSDictionaryOfVariableBindings(betName, betSubtitle);
//    
//    [leftView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[betName]-0-[betSubtitle]|" options:0 metrics:nil views:left]];
//
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[betName]-13-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[betSubtitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betSubtitle)]];
    


    
   
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
