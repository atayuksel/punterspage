//
//  HelpViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 15/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "HelpViewController.h"

#import "SWRevealViewController.h"




@import CoreGraphics;

@interface HelpViewController ()
{
    
}

-(IBAction)pushExample:(id)sender;

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self setupLayout];
}

-(void)setupLayout
    {
        SWRevealViewController *revealController = [self revealViewController];
        
        
        [revealController panGestureRecognizer];
        [revealController tapGestureRecognizer];
        
        //create the view
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor whiteColor];
        
        
        //everything for tabbar
        UINavigationBar *navBar = [[UINavigationBar alloc] init];
        [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];
        
        float rd = 0.00/255.00;
        float gr = 102.00/255.00;
        float bl = 255.00/255.00;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        self.navigationItem.title= @"Help";
        
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        
        [self.view addSubview:navBar];
        
        //add the components to the view
        [view addSubview: navBar];
        
        UIScrollView *scrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height-80)];
        UILabel *help = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 100)];
        NSString *test = @"Coming soon...";
        
        help.text= test;
        help.lineBreakMode = NSLineBreakByWordWrapping;
        help.numberOfLines = 0;
        [help sizeToFit];
        
        [scrollview addSubview:help];
        scrollview.scrollEnabled=YES;
        scrollview.contentSize = CGSizeMake(scrollview.contentSize.width-20, help.frame.size.height);
        //    scrollview.backgroundColor = [UIColor whiteColor];
        //    aboutUs.backgroundColor = [UIColor whiteColor];
        [view addSubview:scrollview];
        
        self.view.backgroundColor= [UIColor whiteColor];
        
//        UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
//                                                                             style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
//        
//        self.navigationItem.leftBarButtonItem = revealButtonItem;
//        revealButtonItem.tintColor = [UIColor whiteColor];
        
        self.view = view;
    }
    
    
    - (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
    }
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    - (IBAction)pushExample:(id)sender
    {
        UIViewController *stubController = [[UIViewController alloc] init];
        stubController.view.backgroundColor = [UIColor whiteColor];
        [self.navigationController pushViewController:stubController animated:YES];
    }



@end
