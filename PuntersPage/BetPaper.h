//
//  BetPaper.h
//  PuntersPage
//
//  Created by Stefan Oancea on 14/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "JSONModel.h"
@protocol BetPaper
@end

@interface BetPaper : JSONModel

@property(nonatomic,retain) NSString *id;
@property(nonatomic,retain) NSString *startDate;
@property(nonatomic,retain) NSString *dueDate;
@property(nonatomic,retain) NSString *typeDetail;

@end
