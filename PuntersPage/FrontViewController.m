//
//  ViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 14/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "FrontViewController.h"
#import "SWRevealViewController.h"
#import "BetCell.h"
#import "BetViewController.h"
#import "Reachability.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIScrollView+SVPullToRefresh.h"
#import "UIScrollView+SVInfiniteScrolling.h"
//#import "UIImageView+WebCache.h"
#import "SGImageCache.h"
#import <Netmera/Netmera.h>





@import CoreGraphics;

@interface FrontViewController () <UITableViewDataSource,UITableViewDelegate >
{
//    KMXMLParserDelegate
    UITableView *bets;
    NSInteger _presentedRow;
    bool contor;
 
}

-(IBAction)pushExample:(id)sender;




@end

@implementation FrontViewController
@synthesize dataArray = _dataArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    NSURL *feedURL = [NSURL URLWithString:@"http://178.62.49.240/backend/web/ws/free-bets"];
    KMXMLParser *parser = [[KMXMLParser alloc] initWithURL:feedURL delegate:self];
    //To get the result and store it in an array call the parser 'posts' method
    self.dataArray = [parser posts];

    
    [self performSelector:@selector(onTick) withObject:nil afterDelay:0.2];

    
//    NSInteger *ceva;
//    ceva= [_dataArray count ];
//    NSLog(@"ai atatea elemente  %ld",(long)ceva);


}
- (void)viewDidAppear:(BOOL)animated {
    [bets triggerPullToRefresh];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [bets reloadData];
    
    [NMEvent sendEventWithKey:@"betmenuclicked" value:nil];
}



- (void)setupLayout
{
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
//    __weak FrontViewController *weakSelf = self;
//    
//    [bets addPullToRefreshWithActionHandler:^{
//        [weakSelf reloadInputViews];
//        
//    }];
//    
//    [bets addInfiniteScrollingWithActionHandler:^{
//        [weakSelf reloadInputViews];
//    }];

    
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];
    
    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title= @"Free Bets";
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.view addSubview:navBar];
    
    //add the components to the view
    [view addSubview: navBar];
    
    
    
    
    
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    revealButtonItem.tintColor = [UIColor whiteColor];
    
    bets = [[UITableView alloc] init];
    bets.delegate = self;
    bets.dataSource = self;
    bets.showsVerticalScrollIndicator = true;
    bets.separatorStyle = UITableViewCellSeparatorStyleNone;
    bets.translatesAutoresizingMaskIntoConstraints = NO;
    view=bets;

    
    
    
    
    //show the view
    self.view = view;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Example Code

- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.dataArray count];
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    NSInteger row=indexPath.row;
    
//    if (tableView.contentSize.height < tableView.frame.size.height-80) {
//        tableView.scrollEnabled = NO;
//    }
//    else {
//        tableView.scrollEnabled = YES;
//    }
    
    
    if (cell == nil)
    {
        cell = [[BetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    
    NSString * urlString = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    NSRange range = [urlString rangeOfString:@"%0A%09%09"];
    
    if (range.location != NSNotFound) {
        urlString = [urlString stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                             withString:@""];
        
    }


    
//    [imgView sd_setImageWithURL:[NSURL URLWithString:urlString]
//                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]
//                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
//     {
//         NSLog(@"Cacheing succesful");
//         
//     }
//     ];
    [SGImageCache getImageForURL:urlString].then(^(UIImage *image) {
        if (image) {
            NSLog(@"in image statement");
            imgView.image = image;
            cell.betImage.image=imgView.image;
        }
    });
    
  //  cell.betImage.image=imgView.image;
    

    
    
    cell.betName.text = [[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    cell.betSubtitle.text= [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"sub-title"];
    
   
    
  //  UIImageView *imgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    
    NSString * urlString2 = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"free-bet"];
    cell.betPrice.text=urlString2;
//    if (contor==NO && indexPath.row == [self.dataArray count] - 1)
//    {
//        [bets reloadData];
//        contor=YES;
//    }
//   
    return cell;
    

   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSString *tmpURL = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"link"];
//    tmpURL = [tmpURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    NSURL *url = [NSURL URLWithString:[tmpURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    BetViewController *BVC = [[BetViewController alloc]init];
    
    BVC.bettitle=[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"] forKey:@"betitle"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"image"] forKey:@"logo"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"url"] forKey:@"link"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"minimum-deposit"] forKey:@"minimum-deposit"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"free-bet"] forKey:@"free-bet"];
    
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"bet-url"] forKey:@"actionUrl"];
    [defaults setObject:[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"text"] forKey:@"text"];

  //  BVC.bettitle=[[self.dataArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    [self.navigationController pushViewController:[BetViewController new] animated:YES];
    


}

+ (BOOL)getConnectivity {
    return [[Reachability reachabilityWithHostName:@"google.com"] currentReachabilityStatus] != NotReachable;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    return 80;
}

- (void)parserDidFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}

- (void)parserCompletedSuccessfully
{
    NSLog(@"Parse complete. You may need to reload the table view to see the data.");
 
    
    
}

- (void)parserDidBegin
{
    NSLog(@"Parsing has begun");
}

-(void)onTick
{
    [bets reloadData];
}

@end
