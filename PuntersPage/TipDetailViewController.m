//
//  TipDetailViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 28/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "TipDetailViewController.h"
#import "SWRevealViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Netmera/Netmera.h>

@import CoreGraphics;
@interface TipDetailViewController ()

@end

@implementation TipDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    // Do any additional setup after loading the view.
    [self performSelector:@selector(onTick) withObject:nil afterDelay:0.50];
    
    [NMEvent sendEventWithKey:@"tipdetailclicked" value:nil];
}

- (void)setupLayout
{
    
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];
    
    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    UIScrollView *myScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(0, -500, self.view.frame.size.width, 650)];
    
    float rd2 = 235.00/255.00;
    float gr2 = 235.00/255.00;
    float bl2 = 235.00/255.00;

    newView.backgroundColor =[UIColor colorWithRed:rd2 green:gr2 blue:bl2 alpha:1.0];
    [myScroll addSubview:newView];
    NSString *logoUrl =[[NSUserDefaults standardUserDefaults] valueForKey:@"tiplogo"];
    logoUrl = [logoUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:logoUrl];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *logo = [UIImage imageWithData:data];
    UIImageView *myimage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
    myimage.image=logo;
    NSLog(@"logo url e: %@", url);
    
    [myScroll addSubview:myimage];
    myimage.backgroundColor=[UIColor clearColor];
    
    NSString *test = [[NSUserDefaults standardUserDefaults] valueForKey:@"tiptitle"];
    self.navigationItem.title= test;
    
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 32,self.view.frame.size.width-120 , 50)];
    
    myLabel.backgroundColor = [UIColor clearColor];
    myLabel.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
    myLabel.textColor=[UIColor blackColor];
    // add long text to label
    myLabel.text = test;
    myLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
    // set line break mode to word wrap
    myLabel.lineBreakMode = NSLineBreakByWordWrapping;
    myLabel.numberOfLines = 0;
    // resize label
    [myLabel sizeToFit];
    
    
    UIButton *claimButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [claimButton addTarget:self
                    action:@selector(onClaimButtonPressed) forControlEvents:UIControlEventTouchUpInside
     ];
    //[claimButton setTitle:@"CLAIM FREE BETS" forState:UIControlStateNormal];
    [claimButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    claimButton.frame = CGRectMake(15, 100, self.view.frame.size.width-30, self.view.frame.size.width/9.5);
    
    UIImage *buttonImage = [UIImage imageNamed:@"place-bet.png"];
    [claimButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [myScroll addSubview:claimButton];
    
//    UILabel *tipBold =[[UILabel alloc] initWithFrame:CGRectMake(10, 160, self.view.frame.size.width-20, 40)];
//    tipBold.backgroundColor = [UIColor clearColor];
//    tipBold.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    tipBold.textColor=[UIColor blackColor];
//    tipBold.text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit.  ";
//    tipBold.lineBreakMode = NSLineBreakByWordWrapping;
//    tipBold.numberOfLines = 0;
//    [myScroll addSubview: tipBold];
//    tipBold.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
    
    UILabel *tipText =[[UILabel alloc] initWithFrame:CGRectMake(10, 160, self.view.frame.size.width-20, 300)];
    tipText.backgroundColor = [UIColor clearColor];
    tipText.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
    tipText.textColor=[UIColor blackColor];
    NSString *description =[[NSUserDefaults standardUserDefaults] valueForKey:@"tiptext"];

    tipText.text = description;
    tipText.lineBreakMode = NSLineBreakByWordWrapping;
    tipText.numberOfLines = 0;
    tipText.font=[UIFont fontWithName:@"ArialMT" size:13];
    [tipText sizeToFit];
    [myScroll addSubview: tipText];

    myScroll.contentSize = CGSizeMake(self.view.frame.size.width,tipText.frame.size.height+200);
    // add myLabel
    [myScroll addSubview:myLabel];
    // add scroll view to main view
    [self.view addSubview:myScroll];
    // release label and scroll view
    myScroll.backgroundColor = [UIColor whiteColor];
    //    myScroll.frame=CGRectMake(0, 0, self.view.frame.size.width, betText.frame.size.height+depContent.frame.size.height+termContent.frame.size.height+800) ;
    view=myScroll;
    
    
    [self performSelector:@selector(onTick) withObject:nil afterDelay:0.01];
    
    self.view = view;



}
- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}

- (void)onClaimButtonPressed
{
    NSString *actionUrl =[[NSUserDefaults standardUserDefaults] valueForKey:@"tipactionUrl"];
    NSRange range2 = [actionUrl rangeOfString:@"%0A%09%09"];
    
    if (range2.location != NSNotFound) {
        actionUrl = [actionUrl stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                                         withString:@""];
        
    }
    actionUrl = [actionUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Claimed : %@",actionUrl);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: actionUrl]];
    
    
    // Netmera Fire Event when place the bet clicked
    [NMEvent sendEventWithKey:@"tip_placethebet"
                        value:@{
                                @"tiptitle" : [[NSUserDefaults standardUserDefaults] valueForKey:@"tiptitle"]
                                }];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)onTick
{
    [self.view setNeedsDisplay];
}

@end
