//
//  PromotionCell.h
//  PuntersPage
//
//  Created by Stefan Oancea on 17/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionCell : UITableViewCell

@property(nonatomic,retain) UIImageView *promImage;
@property(nonatomic,retain) UILabel *promName;
@property(nonatomic,retain) UILabel *promSubtitle;
@property(nonatomic, retain) UIView *line;
@property(nonatomic, retain) UIImageView *arrow;


@end
