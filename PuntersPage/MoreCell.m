//
//  MoreCell.m
//  PuntersPage
//
//  Created by Stefan Oancea on 16/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "MoreCell.h"

@implementation MoreCell
@synthesize moreImage,moreName,arrow,line;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.moreName = [[UILabel alloc] init];
        //        self.manufacturerModel.numberOfLines = 0;
        //        self.manufacturerModel.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.moreName.textAlignment = NSTextAlignmentLeft;
        self.moreName.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
        self.moreName.textColor = [UIColor blackColor];
        self.moreName.translatesAutoresizingMaskIntoConstraints = NO;
        //[self.contentView addSubview:self.manufacturerModel];
        
        self.moreImage = [[UIImageView alloc] init];
        //self.moreImage.image = [UIImage imageNamed:@"arrow"];
        self.moreImage.contentMode = UIViewContentModeScaleAspectFit;
        self.moreImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.moreImage];
        
        self.line = [[UIView alloc] init];
        self.line.backgroundColor =  [UIColor lightGrayColor];
        self.line.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.line];
        
        self.arrow = [[UIImageView alloc] init];
        self.arrow.image = [UIImage imageNamed:@"arrow"];
        self.arrow.contentMode = UIViewContentModeScaleAspectFit;
        self.arrow.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.arrow];

        
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout
{
    UIView *leftView = [[UIView alloc] init];
    //leftView.backgroundColor = [UIColor redColor];
    leftView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:leftView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(leftView, line, arrow);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[leftView]-[line(==1)]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[arrow]-[line]|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[leftView]-[arrow(==10)]-|" options:0 metrics:nil views:dict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:dict]];
    
    [leftView addSubview:self.moreName];
    [leftView addSubview:self.moreImage];

    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:moreImage attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
       [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:moreName attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    

//
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[moreImage]-10-[moreName]-[arrow]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(moreImage,moreName,arrow)]];
//    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[moreImage]-20-[moreName]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(moreImage,moreName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-75-[moreName]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(moreName)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[moreImage]-2-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(moreImage)]];
    //

    
    
}










- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
