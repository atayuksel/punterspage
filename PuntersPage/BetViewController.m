//
//  BetViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 20/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "BetViewController.h"
#import "SWRevealViewController.h"
#import "JKExpandTableView.h"
#import "FrontViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Netmera/Netmera.h>

@import CoreGraphics;

@interface BetViewController () <JKExpandTableViewDataSource, JKExpandTableViewDelegate>

{
    NSMutableArray *parents;
    NSMutableArray *children;
    JKExpandTableView *filterOptions;
}

-(IBAction)pushExample:(id)sender;
@end

@implementation BetViewController
@synthesize dataArray = _dataArray;
@synthesize bettitle = _bettitle;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    // Do any additional setup after loading the view.
     [self performSelector:@selector(onTick) withObject:nil afterDelay:2.00];
    
    [NMEvent sendEventWithKey:@"freebetclick" value:nil];
    
}



- (void)setupLayout
{
    
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];
    
    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    

    
    NSString *test = [[NSUserDefaults standardUserDefaults] valueForKey:@"betitle"];
    self.navigationItem.title= test;
    
    NSString *logoUrl =[[NSUserDefaults standardUserDefaults] valueForKey:@"logo"];
    NSString *description =[[NSUserDefaults standardUserDefaults] valueForKey:@"text"];
    NSRange range = [logoUrl rangeOfString:@"%0A%09%09"];
    
    if (range.location != NSNotFound) {
        logoUrl = [logoUrl stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                                         withString:@""];
        
    }


    


    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.view addSubview:navBar];
    
    //add the components to the view
    [view addSubview: navBar];
    
    
    
    
    
    
//    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
//                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
//    
//    self.navigationItem.leftBarButtonItem = revealButtonItem;
//    revealButtonItem.tintColor = [UIColor whiteColor];


    // alocate and initialize scroll
    UIScrollView *myScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(0, -500, self.view.frame.size.width, 650)];
    float rd2 = 235.00/255.00;
    float gr2 = 235.00/255.00;
    float bl2 = 235.00/255.00;

    newView.backgroundColor =[UIColor colorWithRed:rd2 green:gr2 blue:bl2 alpha:1.0];
    [myScroll addSubview:newView];
    
//    UIImageView *myimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 100)];
//    [myimage sd_setImageWithURL:[NSURL URLWithString:logoUrl]
//placeholderImage:[UIImage imageNamed:@"Perfectcircle.gif"]
//completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
//    {
//        NSLog(@"Caching succesful %@ ",logoUrl);
//        
//    }
//    ];
    logoUrl = [logoUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:logoUrl];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *logo = [UIImage imageWithData:data];
    UIImageView *myimage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 75, 75)];
    myimage.image=logo;
    NSLog(@"logo url e: %@", url);

    [myScroll addSubview:myimage];
    myimage.backgroundColor=[UIColor clearColor];

     NSString *minimumBet =[[NSUserDefaults standardUserDefaults] valueForKey:@"minimum-deposit"];
     NSString *freeB =[[NSUserDefaults standardUserDefaults] valueForKey:@"free-bet"];
    
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(88, 25, 200, 50)];
    
//    myLabel.backgroundColor = [UIColor clearColor];
//    myLabel.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    myLabel.textColor=[UIColor blackColor];
//    // add long text to label
//    myLabel.text = @"Bet";
//    // set line break mode to word wrap
//    myLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    myLabel.numberOfLines = 0;
//    // resize label
//    //[myLabel sizeToFit];
    UILabel *betminim =[[UILabel alloc] initWithFrame:CGRectMake(100, 30, self.view.frame.size.width-120, 50)];
    betminim.backgroundColor = [UIColor clearColor];
    betminim.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
    betminim.textColor=[UIColor blackColor];
    betminim.text = minimumBet;
    betminim.lineBreakMode = NSLineBreakByWordWrapping;
    betminim.numberOfLines = 0;
    [myScroll addSubview: betminim];
    betminim.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
    [betminim sizeToFit];
//
//    UILabel *andGet =[[UILabel alloc] initWithFrame:CGRectMake(147, 26, self.view.frame.size.width-20, 50)];
//    andGet.backgroundColor = [UIColor clearColor];
//    andGet.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    andGet.textColor=[UIColor blackColor];
//    andGet.text = @"& get ";
//    andGet.lineBreakMode = NSLineBreakByWordWrapping;
//    andGet.numberOfLines = 0;
//    [myScroll addSubview: andGet];
//    andGet.font=[UIFont fontWithName:@"ArialMT" size:15];
//    
//    UILabel *fBet =[[UILabel alloc] initWithFrame:CGRectMake(187, 26, self.view.frame.size.width-20, 50)];
//    fBet.backgroundColor = [UIColor clearColor];
//    fBet.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    fBet.textColor=[UIColor blackColor];
//    fBet.text = freeB;
//    fBet.lineBreakMode = NSLineBreakByWordWrapping;
//    fBet.numberOfLines = 0;
//    [myScroll addSubview: fBet];
//    fBet.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
//    
//    UILabel *inFree =[[UILabel alloc] initWithFrame:CGRectMake(220, 26, self.view.frame.size.width-20, 50)];
//    inFree.backgroundColor = [UIColor clearColor];
//    inFree.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    inFree.textColor=[UIColor blackColor];
//    inFree.text = @"in Free Bets";
//    inFree.lineBreakMode = NSLineBreakByWordWrapping;
//    inFree.numberOfLines = 0;
//    [myScroll addSubview: inFree];
//    inFree.font=[UIFont fontWithName:@"ArialMT" size:15];
    
    
    UIButton *claimButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [claimButton addTarget:self
               action:@selector(onClaimButtonPressed) forControlEvents:UIControlEventTouchUpInside
];
    //[claimButton setTitle:@"CLAIM FREE BETS" forState:UIControlStateNormal];
    [claimButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    claimButton.frame = CGRectMake(10, 100, self.view.frame.size.width-20, self.view.frame.size.width/9.5);
    
    UIImage *buttonImage = [UIImage imageNamed:@"click.png"];
    [claimButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [myScroll addSubview:claimButton];
    
    
//    UILabel *freeBot =[[UILabel alloc] initWithFrame:CGRectMake(10, 160, self.view.frame.size.width-20, 40)];
//    freeBot.backgroundColor = [UIColor clearColor];
//    freeBot.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    freeBot.textColor=[UIColor blackColor];
//    freeBot.text = freeB;
//    freeBot.lineBreakMode = NSLineBreakByWordWrapping;
//    freeBot.numberOfLines = 0;
//    [myScroll addSubview: freeBot];
//    freeBot.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
//    
//    UILabel *betBold =[[UILabel alloc] initWithFrame:CGRectMake(45, 160, self.view.frame.size.width-20, 40)];
//    betBold.backgroundColor = [UIColor clearColor];
//    betBold.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    betBold.textColor=[UIColor blackColor];
//    betBold.text = @"Free Bets On";
//    betBold.lineBreakMode = NSLineBreakByWordWrapping;
//    betBold.numberOfLines = 0;
//    [myScroll addSubview: betBold];
//    betBold.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
//    
//    UILabel *footName =[[UILabel alloc] initWithFrame:CGRectMake(142, 160, self.view.frame.size.width-20, 40)];
//    footName.backgroundColor = [UIColor clearColor];
//    footName.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    footName.textColor=[UIColor blackColor];
//    footName.text = test;
//    footName.lineBreakMode = NSLineBreakByWordWrapping;
//    footName.numberOfLines = 0;
//    [myScroll addSubview: footName];
//    footName.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
//    
    
    
    
    
    UILabel *betText =[[UILabel alloc] initWithFrame:CGRectMake(10, 160, self.view.frame.size.width-30, 300)];
    betText.backgroundColor = [UIColor clearColor];
    betText.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
    betText.textColor=[UIColor blackColor];
    betText.text = description;
    betText.lineBreakMode = NSLineBreakByWordWrapping;
    betText.numberOfLines = 0;
    betText.font=[UIFont fontWithName:@"ArialMT" size:13];
    [betText sizeToFit];
    [myScroll addSubview: betText];

    
//    parents = [[NSMutableArray alloc] init];
//    [parents addObject:@"Deposit Methods"];
//    [parents addObject:@"Terms and Conditions"];
//    
//    children = [[NSMutableArray alloc] init];
//    NSMutableArray *child;
//    
//    for (int i = 0; i < parents.count; i++)
//    {
//        child = [[NSMutableArray alloc] init];
//        
//        if (i == 0)
//        {
//            NSString *Deposit=@"Hodor, hodor. Hodor. Hodor, hodor, hodor. Hodor hodor hodor hodor... Hodor hodor hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor... Hodor hodor hodor hodor hodor - hodor hodor! Hodor, hodor. Hodor. Hodor, hodor... Hodor hodor hodor HODOR hodor, hodor hodor hodor! Hodor, hodor; hodor HODOR hodor, hodor hodor; hodor hodor? Hodor hodor HODOR hodor, hodor hodor hodor! </p><p>Hodor hodor; hodor hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor... Hodor hodor hodor... Hodor hodor hodor hodor hodor. Hodor. Hodor hodor HODOR! Hodor hodor hodor... Hodor hodor hodor? Hodor Hodor, hodor. Hodor. Hodor, hodor, hodor. ";
//            int deplength=[Deposit length];
//            int dep1= deplength;
//            int impartitor2 =10;
//            int deprows = dep1/impartitor2;
//       
//     
//            
//            [child addObject: Deposit];
//
//        }
//        else if (i == 1)
//        {
//            NSString *Terms=@"Hodor, hodor. Hodor. Hodor, hodor, hodor. Hodor hodor hodor hodor... Hodor hodor hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor... Hodor hodor hodor hodor hodor - hodor hodor! Hodor, hodor. Hodor. Hodor, hodor... Hodor hodor hodor HODOR hodor, hodor hodor hodor! Hodor, hodor; hodor HODOR hodor, hodor hodor; hodor hodor? Hodor hodor HODOR hodor, hodor hodor hodor! </p><p>Hodor hodor; hodor hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor... Hodor hodor hodor... Hodor hodor hodor hodor hodor. Hodor. Hodor hodor HODOR! Hodor hodor hodor... Hodor hodor hodor? Hodor";
//            [child addObject:Terms];
//            int termlength=Terms.length;
//            int term1= termlength;
//            int impartitor = 35;
//            int termrows= term1 / impartitor;
//
//        }
//        
//        [children addObject:child];
//        
//    }
//    
//    NSLog(@"%@", children);
//    
//    
//    
//    filterOptions = [[JKExpandTableView alloc] initWithFrame:CGRectMake(0, 510, myScroll.frame.size.width ,myScroll.frame.size.height ) dataSource:self tableViewDelegate:self];
//    filterOptions.tableViewDelegate = self;
//    filterOptions.dataSourceDelegate = self;
//    [myScroll addSubview:filterOptions];
    
    
//    
//     UIView *DepositMethods = [[UIView alloc] initWithFrame:CGRectMake(10, 200+betText.frame.size.height, self.view.frame.size.width-20, 200)];
//    
//    UIImageView *cellimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 210+betText.frame.size.height, self.view.frame.size.width, 40)];
//    cellimg.image=[UIImage imageNamed:@"cellRow.png"];
//    [myScroll addSubview:cellimg];
//    
//    
//    
//    UIImageView *plusimg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 220+betText.frame.size.height, 20, 20)];
//    plusimg.image=[UIImage imageNamed:@"plus.png"];
//    [myScroll addSubview:plusimg];
//    
//    UILabel *depTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, 220+betText.frame.size.height, self.view.frame.size.width-20, 20)];
//    depTitle.backgroundColor = [UIColor clearColor];
//    depTitle.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    depTitle.textColor=[UIColor blackColor];
//    depTitle.text = @"Deposit Methods";
//    depTitle.lineBreakMode = NSLineBreakByWordWrapping;
//    depTitle.numberOfLines = 0;
//    [myScroll addSubview: depTitle];
//    depTitle.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
//    
////    UILabel *depContent = [[UILabel alloc] initWithFrame:CGRectMake(10, 230+betText.frame.size.height, self.view.frame.size.width-20, 120)];
////    depContent.backgroundColor = [UIColor clearColor];
////    depContent.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
////    depContent.textColor=[UIColor blackColor];
////    depContent.text = @"Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods Deposit Methods ";
////    depContent.lineBreakMode = NSLineBreakByWordWrapping;
////    depContent.numberOfLines = 0;
////  //  [depContent sizeToFit];
////    [myScroll addSubview: depContent];
////    depContent.font=[UIFont fontWithName:@"ArialMT" size:10];
////    [depContent sizeToFit];
//    
////    UIView *Terms = [[UIView alloc] initWithFrame:CGRectMake(10, 140+betText.frame.size.height+DepositMethods.frame.size.height, self.view.frame.size.width-20, 200)];
//    
//    
//    UIImageView *cellimg2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 255+betText.frame.size.height, self.view.frame.size.width, 40)];
//    cellimg2.image=[UIImage imageNamed:@"cellRow.png"];
//    [myScroll addSubview:cellimg2];
//    
//    
//    
//    UIImageView *plusimg2 = [[UIImageView alloc]initWithFrame:CGRectMake(10, 265+betText.frame.size.height, 20, 20)];
//    plusimg2.image=[UIImage imageNamed:@"plus.png"];
//    [myScroll addSubview:plusimg2];
//
//    
//    
//    UILabel *termTitle = [[UILabel alloc] initWithFrame:CGRectMake(40, 265+betText.frame.size.height, self.view.frame.size.width-50, 20)];
//    termTitle.backgroundColor = [UIColor clearColor];
//    termTitle.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    termTitle.textColor=[UIColor blackColor];
//    termTitle.text = @"Terms and conditions";
//    termTitle.lineBreakMode = NSLineBreakByWordWrapping;
//    termTitle.numberOfLines = 0;
//    [myScroll addSubview: termTitle];
//    termTitle.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
//    
////    UILabel *termContent = [[UILabel alloc] initWithFrame:CGRectMake(10, 165+betText.frame.size.height+depContent.frame.size.height, self.view.frame.size.width-50, 120)];
////    termContent.backgroundColor = [UIColor clearColor];
////    termContent.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
////    termContent.textColor=[UIColor blackColor];
////    termContent.text = @"Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions Terms and conditions ";
////    termContent.lineBreakMode = NSLineBreakByWordWrapping;
////    termContent.numberOfLines = 0;
////    [termContent sizeToFit];
////    [myScroll addSubview: termContent];
////    termContent.font=[UIFont fontWithName:@"ArialMT" size:10];
//    
//   
////    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(10,200+ betText.frame.size.height + DepositMethods.frame.size.height+Terms.frame.size.height , self.view.frame.size.width-20, 40)];
//    
//    UILabel *betFooter =[[UILabel alloc] initWithFrame:CGRectMake(10, 100+betText.frame.size.height+190, 150, 40)];
//    betFooter.backgroundColor = [UIColor clearColor];
//    betFooter.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    betFooter.textColor=[UIColor blackColor];
//    betFooter.text = @"For further details visit";
//    betFooter.lineBreakMode = NSLineBreakByWordWrapping;
//    betFooter.numberOfLines = 0;
//    [myScroll addSubview: betFooter];
//    betFooter.font=[UIFont fontWithName:@"Arial-ItalicMT" size:14];
//    
//    
//    NSString *link =[[NSUserDefaults standardUserDefaults] valueForKey:@"link"];
//    
//    
//    UILabel *betLink =[[UILabel alloc] initWithFrame:CGRectMake(154, 100+betText.frame.size.height+190, 150, 40)];
//    betLink.backgroundColor = [UIColor clearColor];
//    betLink.textAlignment = NSTextAlignmentLeft;// UITextAlignmentCenter, UITextAlignmentLeft
//    betLink.textColor=[UIColor orangeColor];
//    betLink.text = link;
//  //  betLink.lineBreakMode = NSLineBreakByWordWrapping;
//  //  betLink.numberOfLines = 0;
//    //[betLink sizeToFit];
//    [myScroll addSubview: betLink];
//    betLink.font=[UIFont fontWithName:@"Arial-BoldItalicMT" size:14];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBetLinkPressed)];
//    [betLink addGestureRecognizer:tap];
//    betLink.userInteractionEnabled = YES;

//    [myScroll addSubview: footer];
    
  //  [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[betFooter]-[betLink]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(betFooter,betLink)]];
//
//    
//    [myScroll addSubview:DepositMethods];
//    [myScroll addSubview:Terms];
    // set scroll view size
    myScroll.contentSize = CGSizeMake(self.view.frame.size.width,betText.frame.size.height+100 );
    // add myLabel
    [myScroll addSubview:myLabel];
    // add scroll view to main view
    [self.view addSubview:myScroll];
    // release label and scroll view
    myScroll.backgroundColor = [UIColor whiteColor];
//    myScroll.frame=CGRectMake(0, 0, self.view.frame.size.width, betText.frame.size.height+depContent.frame.size.height+termContent.frame.size.height+800) ;
    view=myScroll;


 [self performSelector:@selector(onTick) withObject:nil afterDelay:0.01];

    self.view = view;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}

- (void)onClaimButtonPressed
{
    NSString *actionUrl =[[NSUserDefaults standardUserDefaults] valueForKey:@"actionUrl"];
    NSRange range2 = [actionUrl rangeOfString:@"%0A%09%09"];
    
    if (range2.location != NSNotFound) {
        actionUrl = [actionUrl stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                                         withString:@""];
        
    }
    actionUrl = [actionUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"Claimed : %@",actionUrl);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: actionUrl]];
    
    
    [NMEvent sendEventWithKey:@"onclaimbutton"
                        value:@{
                                @"betname" : [[NSUserDefaults standardUserDefaults] valueForKey:@"betitle"]
                                }];
    
}

-(void)onBetLinkPressed
{
    NSString *actionUrl2 =[[NSUserDefaults standardUserDefaults] valueForKey:@"link"];
    NSRange range3 = [actionUrl2 rangeOfString:@"%0A%09%09"];
    
    if (range3.location != NSNotFound) {
        actionUrl2 = [actionUrl2 stringByReplacingOccurrencesOfString:@"%0A%09%09"
                                                         withString:@""];
        
    }
    actionUrl2 = [actionUrl2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *stringFinal= [NSString stringWithFormat:@"%s%@","http://",actionUrl2];
    NSLog(@"Claimed : %@",actionUrl2);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: stringFinal]];

}
- (BOOL) shouldSupportMultipleSelectableChildrenAtParentIndex:(NSInteger) parentIndex
{
    return NO;
}

- (void) tableView:(UITableView *)tableView didSelectCellAtChildIndex:(NSInteger) childIndex withInParentCellIndex:(NSInteger) parentIndex
{
 
    
}

- (void) tableView:(UITableView *)tableView didDeselectCellAtChildIndex:(NSInteger) childIndex withInParentCellIndex:(NSInteger) parentIndex
{
    
    }


#pragma mark - JKExpandTableViewDataSource

- (NSInteger) numberOfParentCells
{
    return parents.count;
}

- (NSInteger) numberOfChildCellsUnderParentIndex:(NSInteger)parentIndex
{
    return [[children objectAtIndex:parentIndex] count];
}

- (NSString *)labelForParentCellAtIndex:(NSInteger)parentIndex
{
    return [parents objectAtIndex:parentIndex];
}

- (NSString *)labelForCellAtChildIndex:(NSInteger)childIndex withinParentCellIndex:(NSInteger) parentIndex
{
    return [[children objectAtIndex:parentIndex] objectAtIndex:childIndex];
}

- (BOOL)shouldDisplaySelectedStateForCellAtChildIndex:(NSInteger)childIndex withinParentCellIndex:(NSInteger) parentIndex
{
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectParentCellAtIndex:(NSInteger) parentIndex
{
    
}


- (UIImage *) iconForParentCellAtIndex:(NSInteger) parentIndex
{
    return [UIImage imageNamed:@"arrow-icon"];
}

- (BOOL) shouldRotateIconForParentOnToggle
{
    return YES;
}
-(void)onTick
{
    [self.view setNeedsDisplay];
}


@end
