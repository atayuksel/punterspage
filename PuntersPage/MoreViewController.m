//
//  MoreViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 15/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "MoreViewController.h"
#import "SWRevealViewController.h"
#import "MoreCell.h"
#import "HelpViewController.h"
#import "AboutUsViewController.h"
#import <Netmera/Netmera.h>


@import CoreGraphics;

@interface MoreViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *bets;
    NSInteger _presentedRow;
}

-(IBAction)pushExample:(id)sender;

@end

@implementation MoreViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [NMEvent sendEventWithKey:@"morepageclick" value:nil];
}


- (void)setupLayout
{
    
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    //create the view
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    
    //everything for tabbar
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    [navBar setFrame:CGRectMake(0,0,CGRectGetWidth(view.frame),250)];

    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.title= @"More";
    
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.view addSubview:navBar];
    
    //add the components to the view
    [view addSubview: navBar];
    
    
    
    
    
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new-button.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    revealButtonItem.tintColor = [UIColor whiteColor];
    
    
    
    bets = [[UITableView alloc] init];
    bets.delegate = self;
    bets.dataSource = self;
    bets.showsVerticalScrollIndicator = true;
    bets.separatorStyle = UITableViewCellSeparatorStyleNone;
    bets.translatesAutoresizingMaskIntoConstraints = NO;
    view=bets;
    
    
    
    //show the view
    self.view = view;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Example Code

- (IBAction)pushExample:(id)sender
{
    UIViewController *stubController = [[UIViewController alloc] init];
    stubController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:stubController animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSInteger row=indexPath.row;
    


    
    if (cell == nil)
    {
        cell = [[MoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //  [[cell textLabel] setText:@"Ceva"];
  //  cell.moreName.text=@"ceva";
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
//    cell.moreImage.image = [UIImage imageNamed:@"Perfectcircle.gif"];
//    cell.imageView.image = imgView.image;
    //    cell.number.text = [[carList objectAtIndex:indexPath.row] registrationNumber];
    
    
    NSString *text = nil;
    if (row == 0)
    {
        text = @"About Us";
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        cell.moreImage.image = [UIImage imageNamed:@"about-us.png"];
        cell.imageView.image = imgView.image;

    }
    else if (row == 1)
    {
        text = @"Follow Us on Twitter";
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        cell.moreImage.image = [UIImage imageNamed:@"twitter.png"];
        cell.imageView.image = imgView.image;

    }
    else if (row == 2)
    {
        text = @"Watch Us on YouTube";
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        cell.moreImage.image = [UIImage imageNamed:@"youtube.png"];
        cell.imageView.image = imgView.image;

    }
    else if (row == 3)
    {
        text = @"Contact Us";
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        cell.moreImage.image = [UIImage imageNamed:@"contact.png"];
        cell.imageView.image = imgView.image;

    }
    else if (row == 4)
    {
        text = @"Help";
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        cell.moreImage.image = [UIImage imageNamed:@"help.png"];
        cell.imageView.image = imgView.image;

    }
    
    cell.moreName.text = NSLocalizedString( text,nil );

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1)
    {
        // open the Twitter App
        if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=ThePuntersPage"]])
        {
            
            // opening the app didn't work - let's open Safari
            if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/ThePuntersPage"]])
            {
                
                // nothing works - perhaps we're not onlye
                NSLog(@"Nothing works. Punt.");
            }
        }
    }
    else if (indexPath.row==2)
    {
        NSURL *youTubeURL = [NSURL URLWithString:@"youtube://www.youtube.com/ThePuntersPage"];
        if ([[UIApplication sharedApplication] canOpenURL:youTubeURL]) {
            [[UIApplication sharedApplication] openURL:youTubeURL];
        } else {
            NSURL *youTubeWebURL = [NSURL URLWithString:@"https://www.youtube.com/user/ThePuntersPage"];
            [[UIApplication sharedApplication] openURL:youTubeWebURL];
        }
    }
    else if (indexPath.row==3)
    {

        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:info@thepunterspage.com"]];
    }

    
    else if (indexPath.row==4)
    {
        [self.navigationController pushViewController:[HelpViewController new] animated:YES];
    }
    else if (indexPath.row==0)
    {
        [self.navigationController pushViewController:[AboutUsViewController new] animated:YES];
    }
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    return 80;
}



@end
