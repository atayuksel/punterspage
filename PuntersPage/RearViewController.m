
//
//  HelpViewController.m
//  PuntersPage
//
//  Created by Stefan Oancea on 14/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import "RearViewController.h"

#import "SWRevealViewController.h"
#import "FrontViewController.h"
#import "TipsViewController.h"
#import "PromotionsViewController.h"
#import "MoreViewController.h"
#import "HelpViewController.h"
#import "TestViewController.h"



@interface RearViewController()
{
    NSInteger _presentedRow;
}

@end

@implementation RearViewController

@synthesize rearTableView = _rearTableView;


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = NSLocalizedString(@"Meniu", nil);
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    float rd = 0.00/255.00;
    float gr = 102.00/255.00;
    float bl = 255.00/255.00;
    _rearTableView.backgroundColor = [UIColor colorWithRed:rd green:gr blue:bl alpha:1.0];
    
    [self.navigationController setNavigationBarHidden:YES];
}


#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    if (tableView.contentSize.height < tableView.frame.size.height) {
        tableView.scrollEnabled = NO;
    }
    else {
        tableView.scrollEnabled = YES;
    }

    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font= [UIFont fontWithName:@"Arial-BoldMT" size:19];
        float rd2 = 0.00/255.00;
        float gr2 = 102.00/255.00;
        float bl2 = 255.00/255.00;
        cell.backgroundColor = [UIColor colorWithRed: rd2 green: gr2 blue: bl2 alpha: 1.0];

    }
	
    NSString *text = nil;
    if (row == 0)
    {
        text = @"Free Bets";
    }
    else if (row == 1)
    {
        text = @"Tips";
    }
    else if (row == 2)
    {
        text = @"Promotions";
    }
    else if (row == 3)
    {
        text = @"More";
    }

    cell.textLabel.text = NSLocalizedString( text,nil );
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // selecting row
    NSInteger row = indexPath.row;
    
    // if we are trying to push the same row or perform an operation that does not imply frontViewController replacement
    // we'll just set position and return
    
//    if ( row == _presentedRow )
//    {
//        [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
//        return;
//    }


    // otherwise we'll create a new frontViewController and push it with animation

    UIViewController *newFrontController = nil;

    if (row == 0)
    {
        newFrontController = [[FrontViewController alloc] init];
    }
    
    else if (row == 1)
    {
        newFrontController = [[TipsViewController alloc] init];
    }
    
    else if (row == 2)
    {
        newFrontController = [[PromotionsViewController alloc] init];
    }
    
    else if (row == 3)
    {
        newFrontController = [[MoreViewController alloc] init];
    }

    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    [revealController pushFrontViewController:navigationController animated:YES];
    
    _presentedRow = row;  // <- store the presented row
}



//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end