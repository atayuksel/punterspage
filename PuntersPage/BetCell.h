//
//  BetCell.h
//  PuntersPage
//
//  Created by Stefan Oancea on 14/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BetCell : UITableViewCell

@property(nonatomic,retain) UIImageView *betImage;
@property(nonatomic,retain) UILabel *betName;
@property(nonatomic,retain) UILabel *betSubtitle;
@property(nonatomic,retain) UILabel *betPrice;
@property(nonatomic, retain) UIView *line;
@property(nonatomic, retain) UIImageView *arrow;

@end
