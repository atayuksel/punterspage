//
//  TipsViewCell.h
//  PuntersPage
//
//  Created by Stefan Oancea on 17/07/15.
//  Copyright (c) 2015 Customsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipsCell : UITableViewCell

@property(nonatomic,retain) UIImageView *tipImage;
@property(nonatomic,retain) UILabel *tipName;
@property(nonatomic,retain) UILabel *tipSubtitle;
@property(nonatomic, retain) UIView *line;
@property(nonatomic, retain) UIImageView *arrow;


@end
